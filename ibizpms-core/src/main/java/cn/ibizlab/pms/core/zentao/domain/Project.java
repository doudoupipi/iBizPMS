package cn.ibizlab.pms.core.zentao.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.pms.util.domain.EntityBase;
import cn.ibizlab.pms.util.annotation.DEField;
import cn.ibizlab.pms.util.enums.DEPredefinedFieldType;
import cn.ibizlab.pms.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.pms.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.pms.util.domain.EntityMP;

/**
 * 实体[项目]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "zt_project",resultMap = "ProjectResultMap")
public class Project extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前系统版本
     */
    @TableField(value = "openedversion")
    @JSONField(name = "openedversion")
    @JsonProperty("openedversion")
    private String openedversion;
    /**
     * 开始时间
     */
    @TableField(value = "begin")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "begin" , format="yyyy-MM-dd")
    @JsonProperty("begin")
    private Timestamp begin;
    /**
     * 访问控制
     */
    @TableField(value = "acl")
    @JSONField(name = "acl")
    @JsonProperty("acl")
    private String acl;
    /**
     * 已删除
     */
    @DEField(defaultValue = "0" , preType = DEPredefinedFieldType.LOGICVALID, logicval = "0" , logicdelval="1")
    @TableLogic(value= "0",delval="1")
    @TableField(value = "deleted")
    @JSONField(name = "deleted")
    @JsonProperty("deleted")
    private String deleted;
    /**
     * 项目描述
     */
    @TableField(value = "desc")
    @JSONField(name = "desc")
    @JsonProperty("desc")
    private String desc;
    /**
     * 项目负责人
     */
    @TableField(value = "pm")
    @JSONField(name = "pm")
    @JsonProperty("pm")
    private String pm;
    /**
     * 项目编号
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private BigInteger id;
    /**
     * 项目名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 子状态
     */
    @TableField(value = "substatus")
    @JSONField(name = "substatus")
    @JsonProperty("substatus")
    private String substatus;
    /**
     * 项目排序
     */
    @TableField(value = "order")
    @JSONField(name = "order")
    @JsonProperty("order")
    private Integer order;
    /**
     * 发布负责人
     */
    @TableField(value = "rd")
    @JSONField(name = "rd")
    @JsonProperty("rd")
    private String rd;
    /**
     * 分组白名单
     */
    @TableField(value = "whitelist")
    @JSONField(name = "whitelist")
    @JsonProperty("whitelist")
    private String whitelist;
    /**
     * 优先级
     */
    @DEField(defaultValue = "1")
    @TableField(value = "pri")
    @JSONField(name = "pri")
    @JsonProperty("pri")
    private String pri;
    /**
     * 结束日期
     */
    @TableField(value = "end")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "end" , format="yyyy-MM-dd")
    @JsonProperty("end")
    private Timestamp end;
    /**
     * 取消日期
     */
    @TableField(value = "canceleddate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "canceleddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("canceleddate")
    private Timestamp canceleddate;
    /**
     * 项目代号
     */
    @TableField(value = "code")
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;
    /**
     * catID
     */
    @TableField(value = "catid")
    @JSONField(name = "catid")
    @JsonProperty("catid")
    private Integer catid;
    /**
     * statge
     */
    @DEField(defaultValue = "1")
    @TableField(value = "statge")
    @JSONField(name = "statge")
    @JsonProperty("statge")
    private String statge;
    /**
     * 由谁取消
     */
    @TableField(value = "canceledby")
    @JSONField(name = "canceledby")
    @JsonProperty("canceledby")
    private String canceledby;
    /**
     * isCat
     */
    @DEField(defaultValue = "0")
    @TableField(value = "iscat")
    @JSONField(name = "iscat")
    @JsonProperty("iscat")
    private Integer iscat;
    /**
     * 创建日期
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "openeddate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "openeddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("openeddate")
    private Timestamp openeddate;
    /**
     * 由谁关闭
     */
    @TableField(value = "closedby")
    @JSONField(name = "closedby")
    @JsonProperty("closedby")
    private String closedby;
    /**
     * 项目类型
     */
    @DEField(defaultValue = "sprint")
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 产品负责人
     */
    @TableField(value = "po")
    @JSONField(name = "po")
    @JsonProperty("po")
    private String po;
    /**
     * 项目状态
     */
    @TableField(value = "status")
    @JSONField(name = "status")
    @JsonProperty("status")
    private String status;
    /**
     * 可用工作日
     */
    @TableField(value = "days")
    @JSONField(name = "days")
    @JsonProperty("days")
    private Integer days;
    /**
     * 团队名称
     */
    @TableField(value = "team")
    @JSONField(name = "team")
    @JsonProperty("team")
    private String team;
    /**
     * 关闭日期
     */
    @TableField(value = "closeddate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "closeddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("closeddate")
    private Timestamp closeddate;
    /**
     * 由谁创建
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMANNAME)
    @TableField(value = "openedby")
    @JSONField(name = "openedby")
    @JsonProperty("openedby")
    private String openedby;
    /**
     * 测试负责人
     */
    @TableField(value = "qd")
    @JSONField(name = "qd")
    @JsonProperty("qd")
    private String qd;
    /**
     * parent
     */
    @TableField(exist = false)
    @JSONField(name = "parentname")
    @JsonProperty("parentname")
    private String parentname;
    /**
     * 父项目
     */
    @TableField(value = "parent")
    @JSONField(name = "parent")
    @JsonProperty("parent")
    private BigInteger parent;
    /**
     * 任务总数
     */
    @TableField(exist = false)
    @JSONField(name = "taskcnt")
    @JsonProperty("taskcnt")
    private Integer taskcnt;
    /**
     * Bug总数
     */
    @TableField(exist = false)
    @JSONField(name = "bugcnt")
    @JsonProperty("bugcnt")
    private Integer bugcnt;
    /**
     * 需求总数
     */
    @TableField(exist = false)
    @JSONField(name = "storycnt")
    @JsonProperty("storycnt")
    private Integer storycnt;
    /**
     * 关联产品集合
     */
    @TableField(exist = false)
    @JSONField(name = "products")
    @JsonProperty("products")
    private String products;
    /**
     * 关联产品平台集合
     */
    @TableField(exist = false)
    @JSONField(name = "branchs")
    @JsonProperty("branchs")
    private String branchs;
    /**
     * 关联产品产品计划
     */
    @TableField(exist = false)
    @JSONField(name = "plans")
    @JsonProperty("plans")
    private String plans;
    /**
     * 关联数据数组
     */
    @TableField(exist = false)
    @JSONField(name = "srfarray")
    @JsonProperty("srfarray")
    private String srfarray;
    /**
     * 备注
     */
    @TableField(exist = false)
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;
    /**
     * 时间段
     */
    @TableField(exist = false)
    @JSONField(name = "period")
    @JsonProperty("period")
    private String period;
    /**
     * 项目团队成员
     */
    @TableField(exist = false)
    @JSONField(name = "account")
    @JsonProperty("account")
    private String account;
    /**
     * 加盟日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "join" , format="yyyy-MM-dd")
    @JsonProperty("join")
    private Timestamp join;
    /**
     * 可用工时/天
     */
    @TableField(exist = false)
    @JSONField(name = "hours")
    @JsonProperty("hours")
    private Double hours;
    /**
     * 角色
     */
    @TableField(exist = false)
    @JSONField(name = "role")
    @JsonProperty("role")
    private String role;
    /**
     * 任务消耗总工时
     */
    @TableField(exist = false)
    @JSONField(name = "totalconsumed")
    @JsonProperty("totalconsumed")
    private Double totalconsumed;
    /**
     * 总工时
     */
    @TableField(exist = false)
    @JSONField(name = "totalwh")
    @JsonProperty("totalwh")
    private Integer totalwh;
    /**
     * 任务预计剩余总工时
     */
    @TableField(exist = false)
    @JSONField(name = "totalleft")
    @JsonProperty("totalleft")
    private Double totalleft;
    /**
     * 任务最初预计总工时
     */
    @TableField(exist = false)
    @JSONField(name = "totalestimate")
    @JsonProperty("totalestimate")
    private Double totalestimate;
    /**
     * 可用工时
     */
    @TableField(exist = false)
    @JSONField(name = "totalhours")
    @JsonProperty("totalhours")
    private Double totalhours;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.pms.core.zentao.domain.Project ibizparent;



    /**
     * 设置 [当前系统版本]
     */
    public void setOpenedversion(String openedversion){
        this.openedversion = openedversion ;
        this.modify("openedversion",openedversion);
    }

    /**
     * 设置 [开始时间]
     */
    public void setBegin(Timestamp begin){
        this.begin = begin ;
        this.modify("begin",begin);
    }

    /**
     * 格式化日期 [开始时间]
     */
    public String formatBegin(){
        if (this.begin == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(begin);
    }
    /**
     * 设置 [访问控制]
     */
    public void setAcl(String acl){
        this.acl = acl ;
        this.modify("acl",acl);
    }

    /**
     * 设置 [项目描述]
     */
    public void setDesc(String desc){
        this.desc = desc ;
        this.modify("desc",desc);
    }

    /**
     * 设置 [项目负责人]
     */
    public void setPm(String pm){
        this.pm = pm ;
        this.modify("pm",pm);
    }

    /**
     * 设置 [项目名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [子状态]
     */
    public void setSubstatus(String substatus){
        this.substatus = substatus ;
        this.modify("substatus",substatus);
    }

    /**
     * 设置 [项目排序]
     */
    public void setOrder(Integer order){
        this.order = order ;
        this.modify("order",order);
    }

    /**
     * 设置 [发布负责人]
     */
    public void setRd(String rd){
        this.rd = rd ;
        this.modify("rd",rd);
    }

    /**
     * 设置 [分组白名单]
     */
    public void setWhitelist(String whitelist){
        this.whitelist = whitelist ;
        this.modify("whitelist",whitelist);
    }

    /**
     * 设置 [优先级]
     */
    public void setPri(String pri){
        this.pri = pri ;
        this.modify("pri",pri);
    }

    /**
     * 设置 [结束日期]
     */
    public void setEnd(Timestamp end){
        this.end = end ;
        this.modify("end",end);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatEnd(){
        if (this.end == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(end);
    }
    /**
     * 设置 [取消日期]
     */
    public void setCanceleddate(Timestamp canceleddate){
        this.canceleddate = canceleddate ;
        this.modify("canceleddate",canceleddate);
    }

    /**
     * 格式化日期 [取消日期]
     */
    public String formatCanceleddate(){
        if (this.canceleddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(canceleddate);
    }
    /**
     * 设置 [项目代号]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [catID]
     */
    public void setCatid(Integer catid){
        this.catid = catid ;
        this.modify("catid",catid);
    }

    /**
     * 设置 [statge]
     */
    public void setStatge(String statge){
        this.statge = statge ;
        this.modify("statge",statge);
    }

    /**
     * 设置 [由谁取消]
     */
    public void setCanceledby(String canceledby){
        this.canceledby = canceledby ;
        this.modify("canceledby",canceledby);
    }

    /**
     * 设置 [isCat]
     */
    public void setIscat(Integer iscat){
        this.iscat = iscat ;
        this.modify("iscat",iscat);
    }

    /**
     * 设置 [由谁关闭]
     */
    public void setClosedby(String closedby){
        this.closedby = closedby ;
        this.modify("closedby",closedby);
    }

    /**
     * 设置 [项目类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [产品负责人]
     */
    public void setPo(String po){
        this.po = po ;
        this.modify("po",po);
    }

    /**
     * 设置 [项目状态]
     */
    public void setStatus(String status){
        this.status = status ;
        this.modify("status",status);
    }

    /**
     * 设置 [可用工作日]
     */
    public void setDays(Integer days){
        this.days = days ;
        this.modify("days",days);
    }

    /**
     * 设置 [团队名称]
     */
    public void setTeam(String team){
        this.team = team ;
        this.modify("team",team);
    }

    /**
     * 设置 [关闭日期]
     */
    public void setCloseddate(Timestamp closeddate){
        this.closeddate = closeddate ;
        this.modify("closeddate",closeddate);
    }

    /**
     * 格式化日期 [关闭日期]
     */
    public String formatCloseddate(){
        if (this.closeddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(closeddate);
    }
    /**
     * 设置 [测试负责人]
     */
    public void setQd(String qd){
        this.qd = qd ;
        this.modify("qd",qd);
    }

    /**
     * 设置 [父项目]
     */
    public void setParent(BigInteger parent){
        this.parent = parent ;
        this.modify("parent",parent);
    }


}


