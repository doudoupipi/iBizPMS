package cn.ibizlab.pms.core.ibiz.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.pms.core.ibiz.domain.TestModule;

/**
 * 关系型数据实体[FixPath] 对象
 */
public interface ITestModuleFixPathLogic {

    void execute(TestModule testmodule ) ;

}
