package cn.ibizlab.pms.core.zentao.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.pms.core.zentao.domain.Project;

/**
 * 关系型数据实体[UpdateProjectCycle__MSDeny] 对象
 */
public interface IProjectUpdateProjectCycle__MSDenyLogic {

    void execute(Project project ) ;

}
