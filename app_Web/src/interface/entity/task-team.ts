/**
 * 任务团队
 *
 * @export
 * @interface TaskTeam
 */
export interface TaskTeam {

    /**
     * 角色
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    role?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    root?: any;

    /**
     * 受限用户
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    limited?: any;

    /**
     * 总计可用
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    total?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    username?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    order?: any;

    /**
     * 可用工日
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    days?: any;

    /**
     * 团队类型
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    type?: any;

    /**
     * 最初预计
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    estimate?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    account?: any;

    /**
     * 总计消耗
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    consumed?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    id?: any;

    /**
     * 加盟日
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    join?: any;

    /**
     * 可用工时/天
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    hours?: any;

    /**
     * 预计剩余
     *
     * @returns {*}
     * @memberof TaskTeam
     */
    left?: any;
}