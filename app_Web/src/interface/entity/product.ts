/**
 * 产品
 *
 * @export
 * @interface Product
 */
export interface Product {

    /**
     * 测试负责人
     *
     * @returns {*}
     * @memberof Product
     */
    qd?: any;

    /**
     * 访问控制
     *
     * @returns {*}
     * @memberof Product
     */
    acl?: any;

    /**
     * 产品名称
     *
     * @returns {*}
     * @memberof Product
     */
    name?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof Product
     */
    id?: any;

    /**
     * 已删除
     *
     * @returns {*}
     * @memberof Product
     */
    deleted?: any;

    /**
     * 分组白名单
     *
     * @returns {*}
     * @memberof Product
     */
    whitelist?: any;

    /**
     * 发布负责人
     *
     * @returns {*}
     * @memberof Product
     */
    rd?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof Product
     */
    order?: any;

    /**
     * 产品类型
     *
     * @returns {*}
     * @memberof Product
     */
    type?: any;

    /**
     * 产品负责人
     *
     * @returns {*}
     * @memberof Product
     */
    po?: any;

    /**
     * 产品描述	
     *
     * @returns {*}
     * @memberof Product
     */
    desc?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Product
     */
    status?: any;

    /**
     * 由谁创建
     *
     * @returns {*}
     * @memberof Product
     */
    createdby?: any;

    /**
     * 当前系统版本
     *
     * @returns {*}
     * @memberof Product
     */
    createdversion?: any;

    /**
     * 子状态
     *
     * @returns {*}
     * @memberof Product
     */
    substatus?: any;

    /**
     * 产品代号
     *
     * @returns {*}
     * @memberof Product
     */
    code?: any;

    /**
     * 创建日期
     *
     * @returns {*}
     * @memberof Product
     */
    createddate?: any;

    /**
     * 产品线
     *
     * @returns {*}
     * @memberof Product
     */
    linename?: any;

    /**
     * 产品线
     *
     * @returns {*}
     * @memberof Product
     */
    line?: any;

    /**
     * 未解决Bug数
     *
     * @returns {*}
     * @memberof Product
     */
    activebugcnt?: any;

    /**
     * 计划总数
     *
     * @returns {*}
     * @memberof Product
     */
    productplancnt?: any;

    /**
     * 发布总数
     *
     * @returns {*}
     * @memberof Product
     */
    releasecnt?: any;

    /**
     * 激活需求数
     *
     * @returns {*}
     * @memberof Product
     */
    activestorycnt?: any;

    /**
     * 未确认Bug数
     *
     * @returns {*}
     * @memberof Product
     */
    unconfirmbugcnt?: any;

    /**
     * 未关闭Bug数
     *
     * @returns {*}
     * @memberof Product
     */
    notclosedbugcnt?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof Product
     */
    comment?: any;

    /**
     * 相关Bug数
     *
     * @returns {*}
     * @memberof Product
     */
    relatedbugcnt?: any;

    /**
     * 已变更需求
     *
     * @returns {*}
     * @memberof Product
     */
    changedstorycnt?: any;

    /**
     * 草稿需求
     *
     * @returns {*}
     * @memberof Product
     */
    draftstorycnt?: any;

    /**
     * 已关闭需求
     *
     * @returns {*}
     * @memberof Product
     */
    closedstorycnt?: any;

    /**
     * 关联项目数
     *
     * @returns {*}
     * @memberof Product
     */
    relatedprojects?: any;

    /**
     * 文档数
     *
     * @returns {*}
     * @memberof Product
     */
    doccnt?: any;

    /**
     * BUILD数
     *
     * @returns {*}
     * @memberof Product
     */
    buildcnt?: any;

    /**
     * 用例数
     *
     * @returns {*}
     * @memberof Product
     */
    casecnt?: any;
}