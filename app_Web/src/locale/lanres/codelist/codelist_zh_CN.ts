export default {
    ProjectQuickpaketMy: {
        "All": "我的项目",
        "empty": "",
    },
    Bug__status: {
        "active": "激活",
        "resolved": "已解决",
        "closed": "已关闭",
        "empty": "",
    },
    Story__closed_reason: {
        "done": "已完成",
        "subdivided": "已细分",
        "duplicate": "重复",
        "postponed": "延期",
        "willnotdo": "不做",
        "cancel": "已取消",
        "bydesign": "设计如此",
        "empty": "",
    },
    RelatedStory: {
        "empty": "",
    },
    Testcase__stage: {
        "unittest": "单元测试阶段",
        "feature": "功能测试阶段",
        "intergrate": "集成测试阶段",
        "system": "系统测试阶段",
        "smoke": "冒烟测试阶段",
        "bvt": "版本验证阶段",
        "empty": "",
    },
    Task__color: {
        "#3da7f5": "#3da7f5",
        "#75c941": "#75c941",
        "#2dbdb2": "#2dbdb2",
        "#797ec9": "#797ec9",
        "#ffaf38": "#ffaf38",
        "#ff4e3e": "#ff4e3e",
        "empty": "",
    },
    ProductBranch_Cache: {
        "empty": "",
    },
    Casestep__type: {
        "step": "步骤",
        "group": "分组",
        "item": "分组步骤",
        "empty": "",
    },
    BugQuickpacketMy: {
        "TOME": "指派给我",
        "ICREATE": "由我创建",
        "BYME": "由我解决",
        "WAITCLOSED": "由我关闭",
        "empty": "",
    },
    StoryQuickpacketMy: {
        "TOME": "指给我",
        "ICREATE": "由我创建",
        "IREVIEW": "由我评审",
        "ICLOSE": "由我关闭",
        "empty": "",
    },
    Product__acl: {
        "open": "默认设置(有产品视图权限，即可访问)",
        "private": "私有产品(相关负责人和项目团队成员才能访问)",
        "custom": "自定义白名单(团队成员和白名单的成员可以访问)",
        "empty": "",
    },
    BugCodeList2: {
        "All": "所有",
        "active": "未解决",
        "empty": "",
    },
    Project__status: {
        "wait": "未开始",
        "doing": "进行中",
        "suspended": "已挂起",
        "closed": "已关闭",
        "empty": "",
    },
    CurProductPlan: {
        "empty": "",
    },
    Task_quickpacket: {
        "All": "所有",
        "UNCLOSED": "未关闭",
        "TOME": "指派给我",
        "MORE": "更多",
        "NOTSTARTED": "未开始",
        "INPROGRESS": "进行中",
        "UNACCOMPLISHED": "未完成",
        "IFINISHED": "我完成",
        "COMPLETED": "已完成",
        "CLOSED": "已关闭",
        "CANCELLED": "已取消",
        "empty": "",
    },
    UserRealNameTask: {
        "empty": "",
    },
    Pri: {
        "1": "一般",
        "2": "最高",
        "3": "较高",
        "4": "最低",
        "empty": "",
    },
    Beginend_disable: {
        "on": "暂时不设定时间",
        "empty": "",
    },
    CodeList47: {
        "1": "1号",
        "2": "2号",
        "3": "3号",
        "4": "4号",
        "5": "5号",
        "6": "6号",
        "7": "7号",
        "8": "8号",
        "9": "9号",
        "10": "10号",
        "11": "11号",
        "12": "12号",
        "13": "13号",
        "14": "14号",
        "15": "15号",
        "16": "16号",
        "17": "17号",
        "18": "18号",
        "19": "19号",
        "20": "20号",
        "21": "21号",
        "22": "22号",
        "23": "23号",
        "24": "24号",
        "25": "25号",
        "26": "26号",
        "27": "27号",
        "28": "28号",
        "29": "29号",
        "30": "30号",
        "31": "31号",
        "empty": "",
    },
    YesNo: {
        "1": "是",
        "0": "否",
        "empty": "",
    },
    Bug__os: {
        "all": "全部",
        "windows": "Windows",
        "win10": "Windows 10",
        "win8": "Windows 8",
        "win7": "Windows 7",
        "vista": "Windows Vista",
        "winxp": "Windows XP",
        "win2012": "Windows 2012",
        "win2008": "Windows 2008",
        "win2003": "Windows 2003",
        "win2000": "Windows 2000",
        "android": "Android",
        "ios": "IOS",
        "wp8": "WP8",
        "wp7": "WP7",
        "symbian": "Symbian",
        "linux": "Linux",
        "freebsd": "FreeBSD",
        "osx": "OS X",
        "unix": "Unix",
        "others": "其他",
        "empty": "",
    },
    UserRealName: {
        "empty": "",
    },
    ProductBranch: {
        "empty": "",
    },
    TypeAll: {
        "custom": "自定义",
        "bug": "Bug",
        "task": "项目任务",
        "story": "项目需求",
        "cycle": "周期",
        "empty": "",
    },
    CurProductBuild: {
        "empty": "",
    },
    Product__type: {
        "normal": "正常",
        "branch": "多分支",
        "platform": "多平台",
        "empty": "",
    },
    Bug__severity: {
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "empty": "",
    },
    Bug__browser: {
        "all": "全部",
        "ie": "IE系列",
        "ie11": "IE11",
        "ie10": "IE10",
        "ie9": "IE9",
        "ie8": "IE8",
        "ie7": "IE7",
        "ie6": "IE6",
        "chrome": "chrome",
        "firefox": "firefox系列",
        "firefox4": "firefox4",
        "firefox3": "firefox3",
        "firefox2": "firefox2",
        "opera": "opera系列",
        "opera11": "opera11",
        "oprea10": "oprea10",
        "opera9": "opera9",
        "safari": "safari",
        "maxthon": "傲游",
        "uc": "UC",
        "others": "其他",
        "empty": "",
    },
    Todo__status: {
        "wait": "未开始",
        "doing": "进行中",
        "done": "已完成",
        "closed": "已关闭",
        "empty": "",
    },
    Project__type: {
        "sprint": "短期项目",
        "waterfall": "长期项目",
        "ops": "运维项目",
        "empty": "",
    },
    Story__status: {
        "draft": "草稿",
        "active": "激活",
        "closed": "已关闭",
        "changed": "已变更",
        "empty": "",
    },
    Action__read: {
        "0": "0",
        "1": "1",
        "empty": "",
    },
    Module__type: {
        "line": "产品线",
        "story": "需求",
        "task": "任务",
        "doc": "文档目录",
        "case": "测试用例",
        "bug": "Bug",
        "empty": "",
    },
    Task__status: {
        "wait": "未开始",
        "doing": "进行中",
        "done": "已完成",
        "pause": "已暂停",
        "cancel": "已取消",
        "closed": "已关闭",
        "empty": "",
    },
    Story__review_result: {
        "pass": "确认通过",
        "revert": "撤销变更",
        "clarify": "有待明确",
        "reject": "拒绝",
        "empty": "",
    },
    CaseQuickpacketMy: {
        "TOME": "给我的用例",
        "ICREATE": "我建的用例",
        "empty": "",
    },
    YesNo2: {
        "1": "是",
        "0": "否",
        "empty": "",
    },
    BugModule: {
        "empty": "",
    },
    CodeList46: {
        "2": "星期一",
        "3": "星期二",
        "4": "星期三",
        "5": "星期四",
        "6": "星期五",
        "7": "星期六",
        "1": "星期日",
        "empty": "",
    },
    Role: {
        "empty": "",
    },
    TestTask: {
        "empty": "",
    },
    Date_disable: {
        "on": "待定",
        "empty": "",
    },
    Product__status: {
        "normal": "正常",
        "closed": "结束",
        "empty": "",
    },
    Task__type: {
        "design": "设计",
        "devel": "开发",
        "test": "测试",
        "study": "研究",
        "discuss": "讨论",
        "ui": "界面",
        "affair": "事务",
        "misc": "其他",
        "empty": "",
    },
    Testtask__pri: {
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "empty": "",
    },
    Story__pri: {
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "empty": "",
    },
    ProjectProductPlan: {
        "empty": "",
    },
    TestQuickpacket: {
        "ALL": "全部",
        "ICREATE": "待测测试单",
        "IREVIEW": "测试中测试单",
        "ICLOSE": "被阻塞测试单",
        "Tested": "已测测试单",
        "empty": "",
    },
    BugUserRealName: {
        "empty": "",
    },
    Project__acl: {
        "open": "默认设置(有项目视图权限，即可访问)",
        "private": "私有项目(只有项目团队成员才能访问)",
        "custom": "自定义白名单(团队成员和白名单的成员可以访问)",
        "empty": "",
    },
    BeginendDropList: {
        "0600": "06:00",
        "0610": "06:10",
        "0620": "06:20",
        "0630": "06:30",
        "0640": "06:40",
        "0650": "06:50",
        "0700": "07:00",
        "0710": "07:10",
        "0720": "07:20",
        "0730": "07:30",
        "0740": "07:40",
        "0750": "07:50",
        "0800": "08:00",
        "0810": "08:10",
        "0820": "08:20",
        "0830": "08:30",
        "0840": "08:40",
        "0850": "08:50",
        "0900": "09:00",
        "0910": "09:10",
        "0920": "09:20",
        "0930": "09:30",
        "0940": "09:40",
        "0950": "09:50",
        "1000": "10:00",
        "1010": "10:10",
        "1020": "10:20",
        "1030": "10:30",
        "1040": "10:40",
        "1050": "10:50",
        "1100": "11:00",
        "1110": "11:10",
        "1120": "11:20",
        "1130": "11:30",
        "1140": "11:40",
        "1150": "11:50",
        "1200": "12:00",
        "1210": "12:10",
        "1220": "12:20",
        "1230": "12:30",
        "1240": "12:40",
        "1250": "12:50",
        "1300": "13:00",
        "1310": "13:10",
        "1320": "13:20",
        "1330": "13:30",
        "1340": "13:40",
        "1350": "13:50",
        "1400": "14:00",
        "1410": "14:10",
        "1420": "14:20",
        "1430": "14:30",
        "1440": "14:40",
        "1450": "14:50",
        "1500": "15:00",
        "1510": "15:10",
        "1520": "15:20",
        "1530": "15:30",
        "1540": "15:40",
        "1550": "15:50",
        "1600": "16:00",
        "1610": "16:10",
        "1620": "16:20",
        "1630": "16:30",
        "1640": "16:40",
        "1650": "16:50",
        "1700": "17:00",
        "1710": "17:10",
        "1720": "17:20",
        "1730": "17:30",
        "1740": "17:40",
        "1750": "17:50",
        "1800": "18:00",
        "1810": "18:10",
        "1820": "18:20",
        "1830": "18:30",
        "1840": "18:40",
        "1850": "18:50",
        "1900": "19:00",
        "1910": "19:10",
        "1920": "19:20",
        "1930": "19:30",
        "1940": "19:40",
        "1950": "19:50",
        "2000": "20:00",
        "2010": "20:10",
        "2020": "20:20",
        "2030": "20:30",
        "2040": "20:40",
        "2050": "20:50",
        "2100": "21:00",
        "2110": "21:10",
        "2120": "21:20",
        "2130": "21:30",
        "2140": "21:40",
        "2150": "21:50",
        "2200": "22:00",
        "2210": "22:10",
        "2220": "22:20",
        "2230": "22:30",
        "2240": "22:40",
        "2250": "22:50",
        "2300": "23:00",
        "2310": "23:10",
        "2320": "23:20",
        "2330": "23:30",
        "2340": "23:40",
        "2350": "23:50",
        "empty": "",
    },
    Bug__type: {
        "codeerror": "代码错误",
        "config": "配置相关",
        "install": "安装部署",
        "security": "安全相关",
        "performance": "性能问题",
        "standard": "标准规范",
        "automation": "测试脚本",
        "designdefect": "设计缺陷",
        "others": "其他",
        "empty": "",
    },
    Team__type: {
        "project": "项目团队",
        "task": "任务团队",
        "empty": "",
    },
    Product: {
        "empty": "",
    },
    Zt__delta: {
        "7": "一星期",
        "14": "两星期",
        "31": "一个月",
        "62": "两个月",
        "93": "三个月",
        "186": "半年",
        "365": "一年",
        "empty": "按时间段",
    },
    Cycle_enable: {
        "1": "周期",
        "empty": "",
    },
    Action__type: {
        "created": "创建",
        "opened": "创建",
        "changed": "变更了",
        "edited": "编辑了",
        "assigned": "指派了",
        "closed": "关闭了",
        "deleted": "删除了",
        "deletedfile": "删除附件",
        "editfile": "编辑附件",
        "erased": "删除了",
        "undeleted": "还原了",
        "hidden": "隐藏了",
        "commented": "评论了",
        "activated": "激活了",
        "blocked": "阻塞了",
        "resolved": "解决了",
        "reviewed": "评审了",
        "moved": "移动了",
        "confirmed": "确认了需求",
        "bugconfirmed": "确认了",
        "tostory": "转需求",
        "frombug": "转需求",
        "fromlib": "从用例库导入",
        "totask": "转任务",
        "svncommited": "提交代码",
        "gitcommited": "提交代码",
        "linked2plan": "关联计划",
        "unlinkedfromplan": "移除计划",
        "changestatus": "修改状态",
        "marked": "编辑了",
        "linked2project": "关联项目",
        "unlinkedfromproject": "移除项目",
        "unlinkedfrombuild": "移除版本",
        "linked2release": "关联发布",
        "unlinkedfromrelease": "移除发布",
        "linkrelatedbug": "关联了相关Bug",
        "unlinkrelatedbug": "移除了相关Bug",
        "linkrelatedcase": "关联了相关用例",
        "unlinkrelatedcase": "移除了相关用例",
        "linkrelatedstory": "关联了相关需求",
        "unlinkrelatedstory": "移除了相关需求",
        "subdividestory": "细分了需求",
        "unlinkchildstory": "移除了细分需求",
        "started": "开始了",
        "restarted": "继续了",
        "recordestimate": "记录了工时",
        "editestimate": "编辑了工时",
        "canceled": "取消了",
        "finished": "完成了",
        "paused": "暂停了",
        "verified": "验收了",
        "delayed": "延期",
        "suspended": "挂起",
        "login": "登录系统",
        "logout": "退出登录",
        "deleteestimate": "删除了工时",
        "linked2build": "关联了",
        "linked2bug": "关联了",
        "linkchildtask": "关联子任务",
        "unlinkchildrentask": "取消关联子任务",
        "linkparenttask": "关联到父任务",
        "unlinkparenttask": "从父任务取消关联",
        "batchcreate": "批量创建任务",
        "createchildren": "创建子任务",
        "managed": "维护",
        "deletechildrentask": "删除子任务",
        "createchildrenstory": "创建子需求",
        "linkchildstory": "关联子需求",
        "unlinkchildrenstory": "取消关联子需求",
        "linkparentstory": "关联到父需求",
        "unlinkparentstory": "从父需求取消关联",
        "deletechildrenstory": "删除子需求",
        "empty": "",
    },
    CurCaseVersion: {
        "empty": "",
    },
    User__gender: {
        "f": "女",
        "m": "男",
        "empty": "",
    },
    Action__object_type: {
        "product": "产品",
        "story": "需求",
        "productplan": "计划",
        "release": "发布",
        "project": "项目",
        "task": "任务",
        "build": "版本",
        "bug": "Bug",
        "case": "用例",
        "caseresult": "用例结果",
        "stepresult": "用例步骤",
        "testtask": "测试单",
        "user": "用户",
        "doc": "文档",
        "doclib": "文档库",
        "todo": "待办",
        "branch": "分支",
        "module": "模块",
        "testsuite": "套件",
        "caselib": "用例库",
        "testreport": "报告",
        "entry": "应用",
        "webhook": "Webhook",
        "empty": "",
    },
    ProductPlan: {
        "empty": "",
    },
    Task__pri: {
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "empty": "",
    },
    Release__status: {
        "normal": "正常",
        "terminate": "停止维护",
        "empty": "",
    },
    Type: {
        "custom": "自定义",
        "bug": "Bug",
        "task": "项目任务",
        "story": "项目需求",
        "empty": "",
    },
    Bug__quickpacket: {
        "All": "所有",
        "UNCLOSED": "未关闭",
        "ICREATE": "由我创建",
        "TOME": "指派给我",
        "BYME": "由我解决",
        "WAITCLOSED": "待关闭",
        "UNRESOLVED": "未解决",
        "MORE": "更多",
        "UNCONFIRMED": "未确认",
        "UNASSIGNED": "未指派",
        "EXPIREDBUG": "过期Bug",
        "empty": "",
    },
    Task__closed_reason: {
        "done": "已完成",
        "cancel": "已取消",
        "empty": "",
    },
    TodoQuickpacketMy: {
        "All": "所有待办",
        "thisyear": "本年度",
        "Unfinished": "未完",
        "BYME": "待定",
        "cycle": "周期",
        "empty": "",
    },
    TASK_QuickacketMy: {
        "TOME": "指派给我",
        "CANCELLED": "由我创建",
        "IFINISHED": "由我完成",
        "COMPLETED": "由我关闭",
        "CLOSED": "由我取消",
        "empty": "",
    },
    UserRealNameProject: {
        "empty": "",
    },
    Private_choose: {
        "1": "是",
        "empty": "",
    },
    Testcase__type: {
        "feature": "功能测试",
        "performance": "性能测试",
        "config": "配置相关",
        "install": "安装部署",
        "security": "安全相关",
        "interface": "接口测试",
        "unit": "单元测试",
        "other": "其他",
        "empty": "",
    },
    Testcase__status: {
        "wait": "待评审",
        "normal": "正常",
        "blocked": "被阻塞",
        "investigate": "研究中",
        "empty": "",
    },
    Story__quickpacket: {
        "ALL": "所有",
        "UNCLOSED": "未关闭",
        "TOME": "指给我",
        "ICREATE": "我创建",
        "IREVIEW": "我评审",
        "DRAFT": "草稿",
        "MORE": "更多",
        "ICLOSE": "我关闭",
        "ACTIVED": "激活",
        "CHANGED": "已变更",
        "TOBECLOSED": "待关闭",
        "CLOSED": "已关闭",
        "empty": "",
    },
    CycleType: {
        "day": "天",
        "week": "周",
        "month": "月度",
        "empty": "",
    },
    Story__stage: {
        "wait": "未开始",
        "planned": "已计划",
        "projected": "已立项",
        "developing": "研发中",
        "developed": "研发完毕",
        "testing": "测试中",
        "tested": "测试完毕",
        "verified": "已验收",
        "released": "已发布",
        "closed": "已关闭",
        "empty": "",
    },
    CurStory: {
        "empty": "",
    },
    Company__guest: {
        "1": "允许",
        "0": "不允许",
        "empty": "",
    },
    YesNo3: {
        "yes": "是",
        "no": "否",
        "empty": "",
    },
    Story__source: {
        "customer": "客户",
        "user": "用户",
        "po": "产品经理",
        "market": "市场",
        "service": "客服",
        "operation": "运营",
        "support": "技术支持",
        "competitor": "竞争对手",
        "partner": "合作伙伴",
        "dev": "开发人员",
        "tester": "测试人员",
        "bug": "Bug",
        "forum": "论坛",
        "other": "其它",
        "empty": "",
    },
    Testcase__pri: {
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "empty": "",
    },
    StoryStageKane: {
        "projected": "已立项",
        "developing": "研发中",
        "developed": "研发完毕",
        "testing": "测试中",
        "tested": "测试完毕",
        "verified": "已验收",
        "released": "已发布",
        "empty": "",
    },
    Testcase__result: {
        "n/a": "忽略",
        "pass": "通过",
        "fail": "失败",
        "blocked": "阻塞",
        "empty": "",
    },
    UserRealNameTaskTeam: {
        "empty": "",
    },
    Bug__pri: {
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "empty": "",
    },
    Bug__resolution: {
        "bydesign": "设计如此",
        "duplicate": "重复Bug",
        "external": "外部原因",
        "fixed": "已解决",
        "notrepro": "无法重现",
        "postponed": "延期处理",
        "willnotfix": "不予解决",
        "tostory": "转为需求",
        "empty": "",
    },
    Testtask__status: {
        "wait": "未开始",
        "doing": "进行中",
        "done": "已完成",
        "blocked": "被阻塞",
        "empty": "",
    },
};