
export default {
  fields: {
    title: "title",
    id: "id",
    content: "content",
    type: "type",
    account: "account",
    ibizpublic: "public",
  },
};