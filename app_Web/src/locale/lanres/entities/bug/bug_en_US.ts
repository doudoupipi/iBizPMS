
export default {
  fields: {
    severity: "严重程度",
    storyversion: "需求版本",
    linkbug: "相关Bug",
    activateddate: "激活日期",
    assignedto: "指派给",
    resolution: "解决方案",
    lastediteddate: "修改日期",
    result: "result",
    keywords: "关键词",
    closedby: "由谁关闭",
    browser: "浏览器",
    steps: "重现步骤",
    v2: "v2",
    confirmed: "是否确认",
    openedby: "由谁创建",
    activatedcount: "激活次数",
    openeddate: "创建日期",
    closeddate: "关闭日期",
    mailto: "抄送给",
    assigneddate: "指派日期",
    deadline: "截止日期",
    color: "标题颜色",
    resolveddate: "解决日期",
    type: "Bug类型",
    status: "Bug状态",
    openedbuild: "影响版本",
    v1: "v1",
    deleted: "已删除",
    lines: "lines",
    substatus: "子状态",
    id: "Bug编号",
    found: "found",
    resolvedby: "解决者",
    resolvedbuild: "解决版本",
    pri: "优先级",
    os: "操作系统",
    hardware: "hardware",
    lasteditedby: "最后修改者",
    title: "Bug标题",
    productname: "产品",
    projectname: "项目",
    storyname: "相关需求",
    caseversion: "用例版本",
    repotype: "代码类型",
    tostory: "转需求",
    entry: "应用",
    product: "所属产品",
    totask: "转任务",
    plan: "所属计划",
    module: "所属模块",
    branch: "平台/分支",
    duplicatebug: "重复ID",
    repo: "代码",
    story: "相关需求",
    ibizcase: "相关用例",
    project: "所属项目",
    task: "相关任务",
    testtask: "测试单",
    comment: "备注",
    taskname: "相关任务",
    modulename: "模块名称",
    branchname: "平台/分支",
    modulename1: "模块名称",
    files: "附件",
  },
	views: {
		plansubgridview: {
			caption: "Bug",
      		title: "Bug",
		},
		buildsubgridview_new: {
			caption: "Bug",
      		title: "bug表格视图",
		},
		maineditview: {
			caption: "Bug编辑",
      		title: "Bug编辑",
		},
		maindashboardview: {
			caption: "Bug",
      		title: "Bug数据看板视图",
		},
		pickupgridview: {
			caption: "Bug",
      		title: "bug选择表格视图",
		},
		mpickupview: {
			caption: "关联Bug",
      		title: "关联Bug",
		},
		activationview: {
			caption: "激活Bug",
      		title: "激活Bug",
		},
		buildsubgridview_new_9212: {
			caption: "Bug",
      		title: "bug表格视图",
		},
		buglifeeditview9: {
			caption: "Bug",
      		title: "Bug的一生",
		},
		editview: {
			caption: "Bug",
      		title: "Bug",
		},
		mpickupview2: {
			caption: "关联Bug",
      		title: "关联Bug",
		},
		editview_1162: {
			caption: "Bug",
      		title: "Bug",
		},
		testreportsubgridview: {
			caption: "Bug",
      		title: "bug表格视图",
		},
		releasesubgridview_done: {
			caption: "Bug",
      		title: "bug表格视图",
		},
		stepsinfoeditview: {
			caption: "重现步骤",
      		title: "Bug编辑视图",
		},
		confirmview: {
			caption: "确认Bug",
      		title: "确认Bug",
		},
		gridview9_assignedtome: {
			caption: "Bug",
      		title: "Bug表格视图",
		},
		editview_4791: {
			caption: "Bug",
      		title: "Bug",
		},
		pickupgridview_buildlinkresolvedbugs: {
			caption: "Bug",
      		title: "bug选择表格视图",
		},
		releasesubgridview_undone: {
			caption: "Bug",
      		title: "bug表格视图",
		},
		todoeditview: {
			caption: "待办提交",
      		title: "待办提交",
		},
		resolveview: {
			caption: "解决Bug",
      		title: "解决Bug",
		},
		gridview: {
			caption: "Bug",
      		title: "bug表格视图",
		},
		gridview9_storyrelated: {
			caption: "相关Bug",
      		title: "相关Bug",
		},
		gridview9_myassignedtome: {
			caption: "Bug",
      		title: "Bug表格视图",
		},
		mainmygridview: {
			caption: "Bug",
      		title: "bug表格视图",
		},
		closeview: {
			caption: "关闭Bug",
      		title: "关闭Bug",
		},
		assingtoview: {
			caption: "指派Bug",
      		title: "指派Bug",
		},
		dashboardmaineditview9: {
			caption: "Bug",
      		title: "主信息",
		},
		buildsubgridview_done: {
			caption: "Bug",
      		title: "bug表格视图",
		},
	},
	stepsinfo_form: {
		details: {
			druipart1: "", 
			grouppanel1: "附件", 
			group1: "Bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "", 
			title: "", 
			steps: "", 
		},
		uiactions: {
		},
	},
	dashboardmainedit_form: {
		details: {
			druipart1: "", 
			grouppanel6: "历史记录", 
			grouppanel1: "分组面板", 
			grouppanel3: "基本信息", 
			grouppanel4: "项目/需求/任务", 
			grouppanel5: "Bug的一生", 
			grouppanel2: "分组面板", 
			group1: "Bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "Bug编号", 
			title: "Bug标题", 
			steps: "重现步骤", 
			comment: "备注", 
			files: "附件", 
			product: "所属产品", 
			productname: "产品", 
			branch: "", 
			branchname: "平台/分支", 
			module: "所属模块", 
			modulename: "模块名称", 
			plan: "所属计划", 
			type: "Bug类型", 
			severity: "严重程度", 
			pri: "优先级", 
			status: "Bug状态", 
			activatedcount: "激活次数", 
			activateddate: "激活日期", 
			confirmed: "是否确认", 
			assignedto: "当前指派", 
			deadline: "截止日期", 
			os: "操作系统", 
			browser: "浏览器", 
			keywords: "关键词", 
			mailto: "抄送给", 
			project: "所属项目", 
			projectname: "项目", 
			storyname: "相关需求", 
			taskname: "相关任务", 
			openedby: "由谁创建", 
			openedbuild: "影响版本", 
			resolvedby: "由谁解决", 
			resolution: "解决方案", 
			resolvedbuild: "解决版本", 
			closedby: "由谁关闭", 
			lasteditedby: "最后修改者", 
			story: "相关需求", 
			task: "相关任务", 
		},
		uiactions: {
		},
	},
	close_form: {
		details: {
			druipart1: "", 
			grouppanel6: "历史记录", 
			group1: "Bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			comment: "备注", 
			id: "Bug编号", 
		},
		uiactions: {
		},
	},
	resolve_form: {
		details: {
			grouppanel1: "分组面板", 
			druipart1: "", 
			grouppanel6: "历史记录", 
			group1: "Bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			product: "所属产品", 
			id: "Bug编号", 
			resolution: "解决方案", 
			resolvedbuild: "解决版本", 
			resolveddate: "解决日期", 
			assignedto: "指派给", 
			project: "所属项目", 
			files: "附件", 
			comment: "备注", 
		},
		uiactions: {
		},
	},
	assignto_form: {
		details: {
			grouppanel1: "分组面板", 
			druipart1: "", 
			grouppanel6: "历史记录", 
			group1: "Bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			assignedto: "指派给", 
			project: "所属项目", 
			mailto: "抄送给", 
			comment: "备注", 
			id: "Bug编号", 
		},
		uiactions: {
		},
	},
	confirm_form: {
		details: {
			druipart1: "", 
			grouppanel6: "历史记录", 
			group1: "Bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			assignedto: "指派给", 
			type: "Bug类型", 
			pri: "优先级", 
			mailto: "抄送给", 
			comment: "备注", 
			project: "所属项目", 
			id: "Bug编号", 
		},
		uiactions: {
		},
	},
	activation_form: {
		details: {
			grouppanel1: "分组面板", 
			druipart1: "", 
			grouppanel6: "历史记录", 
			group1: "Bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "Bug编号", 
			resolvedbuild: "影响版本", 
			assignedto: "指派给", 
			files: "附件", 
			project: "所属项目", 
			comment: "备注", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			grouppanel1: "分组面板", 
			grouppanel2: "分组面板", 
			group1: "bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			productname: "产品", 
			branch: "", 
			product: "所属产品", 
			modulename: "所属模块", 
			module: "所属模块", 
			project: "所属项目", 
			openedbuild: "影响版本", 
			projectname: "项目", 
			assignedto: "指派给", 
			deadline: "截止日期", 
			type: "Bug类型", 
			os: "操作系统", 
			browser: "浏览器", 
			title: "Bug标题", 
			severity: "严重程度", 
			pri: "优先级", 
			steps: "重现步骤", 
			storyname: "相关需求", 
			taskname: "相关任务", 
			mailto: "抄送给", 
			keywords: "关键词", 
			id: "Bug编号", 
			story: "相关需求", 
			task: "相关任务", 
		},
		uiactions: {
		},
	},
	buildbugnew_form: {
		details: {
			grouppanel1: "分组面板", 
			grouppanel2: "分组面板", 
			group1: "bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			productname: "产品", 
			branch: "", 
			product: "所属产品", 
			modulename: "所属模块", 
			module: "所属模块", 
			project: "所属项目", 
			openedbuild: "影响版本", 
			projectname: "项目", 
			assignedto: "指派给", 
			deadline: "截止日期", 
			type: "Bug类型", 
			os: "操作系统", 
			browser: "浏览器", 
			title: "Bug标题", 
			severity: "严重程度", 
			pri: "优先级", 
			steps: "重现步骤", 
			storyname: "相关需求", 
			taskname: "相关任务", 
			mailto: "抄送给", 
			keywords: "关键词", 
			id: "Bug编号", 
			story: "相关需求", 
			task: "相关任务", 
		},
		uiactions: {
		},
	},
	dashboardmain_form: {
		details: {
			maingroup1: "Bug基本信息", 
			formpage1: "基本信息", 
			grouppanel1: "分组面板", 
			formpage2: "项目/需求/任务", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			product: "所属产品", 
			productname: "产品", 
			branch: "平台/分支", 
			branchname: "平台/分支", 
			modulename1: "模块名称", 
			plan: "所属计划", 
			type: "Bug类型", 
			severity: "严重程度", 
			pri: "优先级", 
			status: "Bug状态", 
			activatedcount: "激活次数", 
			activateddate: "激活日期", 
			confirmed: "是否确认", 
			assignedto: "当前指派", 
			deadline: "截止日期", 
			os: "操作系统", 
			browser: "浏览器", 
			keywords: "关键词", 
			mailto: "抄送给", 
			project: "所属项目", 
			projectname: "项目", 
			story: "相关需求", 
			task: "相关任务", 
			id: "Bug编号", 
		},
		uiactions: {
		},
	},
	dashboardbuglife_form: {
		details: {
			buggroup1: "Bug基本信息", 
			formpage1: "Bug的一生", 
			grouppanel1: "分组面板", 
			formpage2: "其他相关", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			openedby: "由谁创建", 
			openedbuild: "影响版本", 
			resolvedby: "由谁解决", 
			resolvedbuild: "解决版本", 
			resolution: "解决方案", 
			closedby: "由谁关闭", 
			lasteditedby: "最后修改者", 
			linkbug: "相关Bug", 
			id: "Bug编号", 
		},
		uiactions: {
		},
	},
	pendingsubmission_form: {
		details: {
			grouppanel1: "分组面板", 
			grouppanel2: "分组面板", 
			group1: "bug基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "修改日期", 
			srforikey: "", 
			srfkey: "Bug编号", 
			srfmajortext: "Bug标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			productname: "产品", 
			branch: "", 
			product: "所属产品", 
			modulename: "所属模块", 
			module: "所属模块", 
			project: "所属项目", 
			openedbuild: "影响版本", 
			projectname: "项目", 
			assignedto: "指派给", 
			deadline: "截止日期", 
			type: "Bug类型", 
			os: "操作系统", 
			browser: "浏览器", 
			title: "Bug标题", 
			severity: "严重程度", 
			pri: "优先级", 
			steps: "重现步骤", 
			storyname: "相关需求", 
			taskname: "相关任务", 
			mailto: "抄送给", 
			keywords: "关键词", 
			id: "Bug编号", 
			story: "相关需求", 
			task: "相关任务", 
		},
		uiactions: {
		},
	},
	main_plansub_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "Bug标题",
			status: "Bug状态",
			openedby: "由谁创建",
			openeddate: "创建日期",
			assignedto: "指派给",
			uagridcolumn1: "操作",
		},
		uiactions: {
        bug_unlinkbug: "移除关联",
		},
	},
	pickupgird_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "Bug标题",
			openedby: "创建",
			resolvedby: "解决者",
			status: "Bug状态",
		},
		uiactions: {
		},
	},
	main_buildsub2_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "Bug标题",
			status: "状态",
			openedby: "创建",
			openeddate: "创建日期",
			resolvedby: "解决",
			uagridcolumn1: "操作",
		},
		uiactions: {
        bug_unlinkbug_build: "解除关联",
		},
	},
	main_buildsub_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "Bug标题",
			status: "Bug状态",
			openedby: "创建",
			assignedto: "指派",
			resolvedby: "解决者",
			resolution: "方案",
			uagridcolumn1: "操作列",
		},
		uiactions: {
        bug_confirmbug: "确认",
        bug_resolvebug: "解决",
        bug_closebug: "关闭",
        copy: "Copy",
        bug_mainedit: "编辑",
		},
	},
	storyrelated_grid: {
		columns: {
			id: "ID",
			title: "Bug标题",
		},
		uiactions: {
		},
	},
	main_releasesub_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "Bug标题",
			status: "Bug状态",
			openedby: "由谁创建",
			openeddate: "创建日期",
			assignedto: "指派给",
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			id: "ID",
			pri: "P",
			confirmed: "确认",
			title: "Bug标题",
			status: "Bug状态",
			openedby: "由谁创建",
			openeddate: "创建日期",
			assignedto: "指派给",
			resolution: "方案",
			uagridcolumn1: "操作",
			lastediteddate: "修改日期",
			activateddate: "激活日期",
		},
		uiactions: {
        bug_confirmbug: "确认",
        bug_resolvebug: "解决",
        bug_closebug: "关闭",
        bug_mainedit: "编辑",
		},
	},
	main_reportsub_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "Bug标题",
			openedby: "创建",
			resolvedby: "解决者",
			resolveddate: "解决日期",
			status: "Bug状态",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			id: "ID",
			pri: "级别",
			title: "Bug标题",
			status: "状态",
		},
		uiactions: {
		},
	},
	maineditviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
	plansubgridviewtoolbar_toolbar: {
		deuiaction3_planrelationbug: {
			caption: "关联Bug",
			tip: "关联Bug",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
	buildsubgridview_donetoolbar_toolbar: {
		deuiaction1: {
			caption: "关联bug",
			tip: "关联bug",
		},
	},
	buildsubgridview_newtoolbar_toolbar: {
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	editview_4791toolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
	buildsubgridview_new_9212toolbar_toolbar: {
		deuiaction1: {
			caption: "提Bug",
			tip: "提Bug",
		},
	},
	releasesubgridview_donetoolbar_toolbar: {
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	releasesubgridview_undonetoolbar_toolbar: {
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		deuiaction4: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	testreportsubgridviewtoolbar_toolbar: {
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	todoeditviewtoolbar_toolbar: {
	},
	editview_1162toolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
};