export default {
  fields: {
    canceledby: "由谁取消",
    left: "预计剩余",
    openeddate: "创建日期",
    color: "标题颜色",
    id: "编号",
    finishedby: "由谁完成",
    finishedlist: "完成者列表",
    realstarted: "实际开始",
    closedby: "由谁关闭",
    substatus: "子状态",
    closedreason: "关闭原因",
    lastediteddate: "最后修改日期",
    assigneddate: "指派日期",
    pri: "优先级",
    lasteditedby: "最后修改",
    status: "任务状态",
    name: "任务名称",
    closeddate: "关闭时间",
    type: "任务类型",
    assignedto: "指派给",
    desc: "任务描述",
    eststarted: "预计开始",
    deadline: "截止日期",
    deleted: "已删除",
    mailto: "抄送给",
    consumed: "总计消耗",
    estimate: "最初预计",
    openedby: "由谁创建",
    canceleddate: "取消时间",
    finisheddate: "实际完成",
    modulename: "所属模块",
    storyname: "相关需求",
    projectname: "所属项目",
    product: "产品",
    storyversion: "需求版本",
    productname: "产品",
    parentname: "父任务",
    project: "所属项目",
    story: "相关需求",
    parent: "父任务",
    frombug: "来源Bug",
    duration: "持续时间",
    module: "id",
    path: "模块路径",
    comment: "备注",
    currentconsumed: "本次消耗",
    totaltime: "总计耗时",
    isleaf: "是否子任务",
    allmodules: "所有模块",
    multiple: "多人任务",
    taskteams: "项目团队成员",
    modulename1: "所属模块",
    ibztaskestimates: "工时",
  },
	views: {
		subtasknewview: {
			caption: "子任务",
      		title: "子任务",
		},
	},
	subtasknew_grid: {
		columns: {
			modulename: "所属模块",
			storyname: "相关需求",
			name: "任务名称",
			type: "任务类型",
			assignedto: "指派给",
			estimate: "预计",
			eststarted: "预计开始",
			deadline: "截止日期",
			desc: "任务描述",
			pri: "优先级",
		},
		uiactions: {
		},
	},
	subtasknewviewtoolbar_toolbar: {
		deuiaction2: {
			caption: "新建行",
			tip: "新建行",
		},
		deuiaction3: {
			caption: "保存行",
			tip: "保存行",
		},
	},
};