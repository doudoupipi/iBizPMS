export default {
  fields: {
    qd: "测试负责人",
    acl: "访问控制",
    name: "产品名称",
    id: "编号",
    deleted: "已删除",
    whitelist: "分组白名单",
    rd: "发布负责人",
    order: "排序",
    type: "产品类型",
    po: "产品负责人",
    desc: "产品描述	",
    status: "状态",
    createdby: "由谁创建",
    createdversion: "当前系统版本",
    substatus: "子状态",
    code: "产品代号",
    createddate: "创建日期",
    linename: "产品线",
    line: "产品线",
    activebugcnt: "未解决Bug数",
    productplancnt: "计划总数",
    releasecnt: "发布总数",
    activestorycnt: "激活需求数",
    unconfirmbugcnt: "未确认Bug数",
    notclosedbugcnt: "未关闭Bug数",
    comment: "备注",
    relatedbugcnt: "相关Bug数",
    changedstorycnt: "已变更需求",
    draftstorycnt: "草稿需求",
    closedstorycnt: "已关闭需求",
    relatedprojects: "关联项目数",
    doccnt: "文档数",
    buildcnt: "BUILD数",
    casecnt: "用例数",
  },
	views: {
		testtabexpview: {
			caption: "测试",
      		title: "测试",
		},
		expeditview: {
			caption: "产品",
      		title: "产品编辑视图",
		},
		htmlview: {
			caption: "iBiz软件生产管理",
      		title: "iBiz软件生产管理",
		},
		projectgridview9: {
			caption: "项目",
      		title: "产品表格视图（项目）",
		},
		testdashboardview: {
			caption: "产品",
      		title: "产品测试数据看板视图",
		},
		testgridview: {
			caption: "产品",
      		title: "产品表格视图",
		},
		chartview: {
			caption: "产品",
      		title: "产品图表视图",
		},
		editview_close: {
			caption: "关闭产品",
      		title: "关闭产品",
		},
		gridview_unclosed: {
			caption: "未关闭产品",
      		title: "产品表格视图",
		},
		storytreeexpview: {
			caption: "产品",
      		title: "产品需求导航视图",
		},
		casetreeexpview: {
			caption: "产品",
      		title: "产品需求导航视图",
		},
		maintabexpview: {
			caption: "产品",
      		title: "产品",
		},
		testleftsidebarlistview: {
			caption: "测试",
      		title: "所有测试",
		},
		gridview: {
			caption: "所有产品",
      		title: "所有产品",
		},
		pickupview: {
			caption: "产品",
      		title: "产品数据选择视图",
		},
		leftsidebarlistview: {
			caption: "产品",
      		title: "所有产品",
		},
		pickupgridview: {
			caption: "产品",
      		title: "产品选择表格视图",
		},
		dashboardinfomainview9: {
			caption: "产品",
      		title: "产品信息",
		},
		maindashboardview: {
			caption: "产品",
      		title: "产品数据看板视图",
		},
		mainview_edit: {
			caption: "产品基本信息",
      		title: "产品编辑视图",
		},
		testlistexpview: {
			caption: "产品",
      		title: "测试统计",
		},
		mytesttabexpview: {
			caption: "测试",
      		title: "测试",
		},
		editview: {
			caption: "产品",
      		title: "产品编辑视图",
		},
		listexpview: {
			caption: "产品统计",
      		title: "产品列表导航视图",
		},
		bugtreeexpview: {
			caption: "产品",
      		title: "产品需求导航视图",
		},
	},
	close_form: {
		details: {
			druipart1: "", 
			group1: "基本信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			comment: "备注", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	main_edit_form: {
		details: {
			grouppanel2: "分组面板", 
			button1: "维护产品线", 
			grouppanel3: "分组面板", 
			grouppanel1: "分组面板", 
			group1: "product基本信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "产品名称", 
			code: "产品代号", 
			line: "产品线", 
			linename: "产品线", 
			po: "产品负责人", 
			qd: "测试负责人", 
			rd: "发布负责人", 
			type: "产品类型", 
			status: "状态", 
			desc: "产品描述	", 
			acl: "访问控制", 
			id: "编号", 
		},
		uiactions: {
			product_seline: "维护产品线",
		},
	},
	dashboardinfo_form: {
		details: {
			group1: "产品基本信息", 
			grouppanel2: "负责人", 
			grouppanel3: "基本信息", 
			grouppanel4: "其他信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "名称", 
			po: "产品", 
			rd: "发布", 
			qd: "测试", 
			linename: "产品线", 
			createdby: "创建人", 
			createddate: "创建日期", 
			acl: "访问控制", 
			activestorycnt: "激活需求", 
			productplancnt: "计划数", 
			relatedbugcnt: "相关Bug", 
			changedstorycnt: "已变更需求", 
			relatedprojects: "关联项目数", 
			casecnt: "用例数", 
			draftstorycnt: "草稿需求", 
			buildcnt: "BUILD数", 
			doccnt: "文档数", 
			closedstorycnt: "已关闭需求", 
			releasecnt: "发布数", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			grouppanel3: "分组面板", 
			button1: "维护产品线", 
			grouppanel2: "分组面板", 
			grouppanel1: "分组面板", 
			group1: "product基本信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "产品名称", 
			code: "产品代号", 
			line: "产品线", 
			linename: "产品线", 
			po: "产品负责人", 
			qd: "测试负责人", 
			rd: "发布负责人", 
			type: "产品类型", 
			desc: "产品描述	", 
			acl: "访问控制", 
			id: "编号", 
		},
		uiactions: {
			product_seline: "维护产品线",
		},
	},
	mainproject_grid: {
		columns: {
			name: "产品名称",
			code: "产品代号",
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			name: "产品名称",
			code: "产品代号",
			status: "状态",
			type: "产品类型",
			linename: "产品线",
		},
		uiactions: {
		},
	},
	test_grid: {
		columns: {
			name: "产品名称",
			code: "产品代号",
			status: "状态",
			type: "产品类型",
			linename: "产品线",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			name: "产品名称",
			code: "产品代号",
			status: "状态",
			type: "产品类型",
			linename: "产品线",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_acl_eq: "访问控制(等于(=))", 
			n_line_eq: "产品线(等于(=))", 
			n_linename_like: "产品线(文本包含(%))", 
			n_linename_eq: "产品线(等于(=))", 
			n_name_like: "产品名称(文本包含(%))", 
		},
		uiactions: {
		},
	},
	editview_closetoolbar_toolbar: {
		deuiaction1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
	},
	mainview_edittoolbar_toolbar: {
		deuiaction1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
	},
	testgridviewtoolbar_toolbar: {
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem16: {
			caption: "其它",
			tip: "其它",
		},
		tbitem21: {
			caption: "导出数据模型",
			tip: "导出数据模型",
		},
	},
	testlistexpviewlistexpbar_list_quicktoolbar_toolbar: {
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
	},
	listexpviewlistexpbar_list_quicktoolbar_toolbar: {
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
	},
	testleftsidebarlistviewtoolbar_toolbar: {
		deuiaction3_testmanager: {
			caption: "管理",
			tip: "管理",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction4: {
			caption: "删除",
			tip: "删除",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	leftsidebarlistviewtoolbar_toolbar: {
		deuiaction3_manager: {
			caption: "管理",
			tip: "管理",
		},
		seperator2: {
			caption: "",
			tip: "",
		},
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
};