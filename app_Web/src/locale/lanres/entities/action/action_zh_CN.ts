export default {
  fields: {
    extra: "附加值",
    objecttype: "对象类型",
    id: "id",
    comment: "备注",
    read: "已读",
    action: "动作",
    date: "日期",
    product: "产品",
    objectid: "对象ID",
    actor: "操作者",
    project: "项目",
    lastcomment: "备注",
  },
	views: {
		myalltrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		historylistview: {
			caption: "系统日志",
      		title: "历史记录",
		},
		projecttrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		projecttrendslistview9: {
			caption: "系统日志",
      		title: "产品动态",
		},
		producttrendslistview9: {
			caption: "系统日志",
      		title: "产品动态",
		},
		alltrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		producttrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		editview: {
			caption: "备注",
      		title: "备注",
		},
	},
	main_form: {
		details: {
			button1: "关闭", 
			button2: "保存", 
			grouppanel1: "分组面板", 
			grouppanel2: "分组面板", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "id", 
			srfmajortext: "备注", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			lastcomment: "", 
			id: "id", 
		},
		uiactions: {
			exit: "返回",
			saveandexit: "保存并关闭",
		},
	},
};