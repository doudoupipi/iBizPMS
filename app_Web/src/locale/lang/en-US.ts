import productplan_en_US from '@locale/lanres/entities/product-plan/product-plan_en_US';
import projectproduct_en_US from '@locale/lanres/entities/project-product/project-product_en_US';
import case_en_US from '@locale/lanres/entities/case/case_en_US';
import ibztaskteam_en_US from '@locale/lanres/entities/ibztask-team/ibztask-team_en_US';
import product_en_US from '@locale/lanres/entities/product/product_en_US';
import taskteam_en_US from '@locale/lanres/entities/task-team/task-team_en_US';
import file_en_US from '@locale/lanres/entities/file/file_en_US';
import suitecase_en_US from '@locale/lanres/entities/suite-case/suite-case_en_US';
import burn_en_US from '@locale/lanres/entities/burn/burn_en_US';
import subproductplan_en_US from '@locale/lanres/entities/sub-product-plan/sub-product-plan_en_US';
import storyspec_en_US from '@locale/lanres/entities/story-spec/story-spec_en_US';
import usertpl_en_US from '@locale/lanres/entities/user-tpl/user-tpl_en_US';
import branch_en_US from '@locale/lanres/entities/branch/branch_en_US';
import productstats_en_US from '@locale/lanres/entities/product-stats/product-stats_en_US';
import ibzprojectmember_en_US from '@locale/lanres/entities/ibz-project-member/ibz-project-member_en_US';
import action_en_US from '@locale/lanres/entities/action/action_en_US';
import group_en_US from '@locale/lanres/entities/group/group_en_US';
import casestep_en_US from '@locale/lanres/entities/case-step/case-step_en_US';
import dept_en_US from '@locale/lanres/entities/dept/dept_en_US';
import company_en_US from '@locale/lanres/entities/company/company_en_US';
import ibzcasestep_en_US from '@locale/lanres/entities/ibzcase-step/ibzcase-step_en_US';
import taskestimate_en_US from '@locale/lanres/entities/task-estimate/task-estimate_en_US';
import story_en_US from '@locale/lanres/entities/story/story_en_US';
import todo_en_US from '@locale/lanres/entities/todo/todo_en_US';
import subtask_en_US from '@locale/lanres/entities/sub-task/sub-task_en_US';
import project_en_US from '@locale/lanres/entities/project/project_en_US';
import history_en_US from '@locale/lanres/entities/history/history_en_US';
import user_en_US from '@locale/lanres/entities/user/user_en_US';
import doclib_en_US from '@locale/lanres/entities/doc-lib/doc-lib_en_US';
import productmodule_en_US from '@locale/lanres/entities/product-module/product-module_en_US';
import module_en_US from '@locale/lanres/entities/module/module_en_US';
import testmodule_en_US from '@locale/lanres/entities/test-module/test-module_en_US';
import productlife_en_US from '@locale/lanres/entities/product-life/product-life_en_US';
import task_en_US from '@locale/lanres/entities/task/task_en_US';
import build_en_US from '@locale/lanres/entities/build/build_en_US';
import testresult_en_US from '@locale/lanres/entities/test-result/test-result_en_US';
import testsuite_en_US from '@locale/lanres/entities/test-suite/test-suite_en_US';
import projectteam_en_US from '@locale/lanres/entities/project-team/project-team_en_US';
import testtask_en_US from '@locale/lanres/entities/test-task/test-task_en_US';
import ibztaskestimate_en_US from '@locale/lanres/entities/ibztask-estimate/ibztask-estimate_en_US';
import productline_en_US from '@locale/lanres/entities/product-line/product-line_en_US';
import testreport_en_US from '@locale/lanres/entities/test-report/test-report_en_US';
import projectstats_en_US from '@locale/lanres/entities/project-stats/project-stats_en_US';
import testrun_en_US from '@locale/lanres/entities/test-run/test-run_en_US';
import ibzmyterritory_en_US from '@locale/lanres/entities/ibz-my-territory/ibz-my-territory_en_US';
import bug_en_US from '@locale/lanres/entities/bug/bug_en_US';
import projectmodule_en_US from '@locale/lanres/entities/project-module/project-module_en_US';
import ibzdoc_en_US from '@locale/lanres/entities/ibz-doc/ibz-doc_en_US';
import release_en_US from '@locale/lanres/entities/release/release_en_US';
import dynadashboard_en_US from '@locale/lanres/entities/dyna-dashboard/dyna-dashboard_en_US';
import components_en_US from '@locale/lanres/components/components_en_US';
import codelist_en_US from '@locale/lanres/codelist/codelist_en_US';
import userCustom_en_US from '@locale/lanres/userCustom/userCustom_en_US';

export default {
    app: {
        commonWords:{
            error: "Error",
            success: "Success",
            ok: "OK",
            cancel: "Cancel",
            save: "Save",
            codeNotExist: 'Code list does not exist',
            reqException: "Request exception",
            sysException: "System abnormality",
            warning: "Warning",
            wrong: "Error",
            rulesException: "Abnormal value check rule",
            saveSuccess: "Saved successfully",
            saveFailed: "Save failed",
            deleteSuccess: "Successfully deleted!",
            deleteError: "Failed to delete",
            delDataFail: "Failed to delete data",
            noData: "No data",
            startsuccess:"Start successful"
        },
        local:{
            new: "New",
            add: "Add",
        },
        gridpage: {
            choicecolumns: "Choice columns",
            refresh: "refresh",
            show: "Show",
            records: "records",
            totle: "totle",
            noData: "No data",
            valueVail: "Value cannot be empty",
            notConfig: {
                fetchAction: "The view table fetchaction parameter is not configured",
                removeAction: "The view table removeaction parameter is not configured",
                createAction: "The view table createaction parameter is not configured",
                updateAction: "The view table updateaction parameter is not configured",
                loaddraftAction: "The view table loadtrafaction parameter is not configured",
            },
            data: "Data",
            delDataFail: "Failed to delete data",
            delSuccess: "Delete successfully!",
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
            notBatch: "Batch addition not implemented",
            grid: "Grid",
            exportFail: "Data export failed",
            sum: "Total",
            formitemFailed: "Form item update failed",
        },
        list: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
                createAction: "View list createAction parameter is not configured",
                updateAction: "View list updateAction parameter is not configured",
            },
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
        },
        listExpBar: {
            title: "List navigation bar",
        },
        wfExpBar: {
            title: "Process navigation bar",
        },
        calendarExpBar:{
            title: "Calendar navigation bar",
        },
        treeExpBar: {
            title: "Tree view navigation bar",
        },
        portlet: {
            noExtensions: "No extensions",
        },
        tabpage: {
            sureclosetip: {
                title: "Close warning",
                content: "Form data Changed, are sure close?",
            },
            closeall: "Close all",
            closeother: "Close other",
        },
        fileUpload: {
            caption: "Upload",
        },
        searchButton: {
            search: "Search",
            reset: "Reset",
        },
        calendar:{
          today: "today",
          month: "month",
          week: "week",
          day: "day",
          list: "list",
          dateSelectModalTitle: "select the time you wanted",
          gotoDate: "goto",
          from: "From",
          to: "To",
        },
        // 非实体视图
        views: {
            ibizpms: {
                caption: "iBiz软件生产管理",
                title: "iBiz软件生产管理",
            },
            productportalview: {
                caption: "产品主页",
                title: "产品主页",
            },
            projectportalview: {
                caption: "项目主页",
                title: "项目主页",
            },
            testportalview: {
                caption: "测试主页",
                title: "测试主页",
            },
        },
        utilview:{
            importview:"Import Data",
            warning:"warning",
            info:"Please configure the data import item"    
        },
        menus: {
            zentao: {
                top_menus: "顶部菜单",
                menuitem9: "我的地盘",
                menuitem3: "产品主页",
                menuitem2: "项目主页",
                menuitem1: "测试主页",
                menuitem7: "iBiz软件生产管理",
                left_exp: "左侧分页导航",
                menuitem4: "产品",
                menuitem5: "项目",
                menuitem6: "测试",
                bottom_exp: "底部导航区",
                footer_center: "底部中间菜单",
            },
        },
        formpage:{
            error: "Error",
            desc1: "Operation failed, failed to find current form item",
            desc2: "Can't continue",
            notconfig: {
                loadaction: "View form loadAction parameter is not configured",
                loaddraftaction: "View form loaddraftAction parameter is not configured",
                actionname: "View form actionName parameter is not configured",
                removeaction: "View form removeAction parameter is not configured",
            },
            saveerror: "Error saving data",
            savecontent: "The data is inconsistent. The background data may have been modified. Do you want to reload the data?",
            valuecheckex: "Value rule check exception",
            savesuccess: "Saved successfully!",
            deletesuccess: "Successfully deleted!",  
            workflow: {
                starterror: "Workflow started successfully",
                startsuccess: "Workflow failed to start",
                submiterror: "Workflow submission failed",
                submitsuccess: "Workflow submitted successfully",
            },
            updateerror: "Form item update failed",       
        },
        gridBar: {
            title: "Table navigation bar",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "View multi-edit view panel fetchAction parameter is not configured",
                loaddraftAction: "View multi-edit view panel loaddraftAction parameter is not configured",
            },
        },
        dataViewExpBar: {
            title: "Card view navigation bar",
        },
        kanban: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
            },
            delete1: "Confirm to delete ",
            delete2: "the delete operation will be unrecoverable!",
        },
        dashBoard: {
            handleClick: {
                title: "Panel design",
            },
        },
        dataView: {
            sum: "total",
            data: "data",
        },
        chart: {
            undefined: "Undefined",
            quarter: "Quarter",   
            year: "Year",
        },
        searchForm: {
            notConfig: {
                loadAction: "View search form loadAction parameter is not configured",
                loaddraftAction: "View search form loaddraftAction parameter is not configured",
            },
            custom: "Store custom queries",
            title: "Name",
        },
        wizardPanel: {
            back: "Back",
            next: "Next",
            complete: "Complete",
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "Dear customer, you have successfully exited the system, after",
                prompt2: "seconds, we will jump to the",
                logingPage: "login page",
            },
            appWfstepTraceView: {
                title: "Application process processing record view",
            },
            appWfstepDataView: {
                title: "Application process tracking view",
            },
            appLoginView: {
                username: "Username",
                password: "Password",
                login: "Login",
            },
        },
    },
    form: {
        group: {
            show_more: "Show More",
            hidden_more: "Hide More"
        }
    },
    entities: {
        productplan: productplan_en_US,
        projectproduct: projectproduct_en_US,
        case: case_en_US,
        ibztaskteam: ibztaskteam_en_US,
        product: product_en_US,
        taskteam: taskteam_en_US,
        file: file_en_US,
        suitecase: suitecase_en_US,
        burn: burn_en_US,
        subproductplan: subproductplan_en_US,
        storyspec: storyspec_en_US,
        usertpl: usertpl_en_US,
        branch: branch_en_US,
        productstats: productstats_en_US,
        ibzprojectmember: ibzprojectmember_en_US,
        action: action_en_US,
        group: group_en_US,
        casestep: casestep_en_US,
        dept: dept_en_US,
        company: company_en_US,
        ibzcasestep: ibzcasestep_en_US,
        taskestimate: taskestimate_en_US,
        story: story_en_US,
        todo: todo_en_US,
        subtask: subtask_en_US,
        project: project_en_US,
        history: history_en_US,
        user: user_en_US,
        doclib: doclib_en_US,
        productmodule: productmodule_en_US,
        module: module_en_US,
        testmodule: testmodule_en_US,
        productlife: productlife_en_US,
        task: task_en_US,
        build: build_en_US,
        testresult: testresult_en_US,
        testsuite: testsuite_en_US,
        projectteam: projectteam_en_US,
        testtask: testtask_en_US,
        ibztaskestimate: ibztaskestimate_en_US,
        productline: productline_en_US,
        testreport: testreport_en_US,
        projectstats: projectstats_en_US,
        testrun: testrun_en_US,
        ibzmyterritory: ibzmyterritory_en_US,
        bug: bug_en_US,
        projectmodule: projectmodule_en_US,
        ibzdoc: ibzdoc_en_US,
        release: release_en_US,
        dynadashboard: dynadashboard_en_US,
    },
    components: components_en_US,
    codelist: codelist_en_US,
    userCustom: userCustom_en_US,
};