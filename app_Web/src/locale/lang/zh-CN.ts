import productplan_zh_CN from '@locale/lanres/entities/product-plan/product-plan_zh_CN';
import projectproduct_zh_CN from '@locale/lanres/entities/project-product/project-product_zh_CN';
import case_zh_CN from '@locale/lanres/entities/case/case_zh_CN';
import ibztaskteam_zh_CN from '@locale/lanres/entities/ibztask-team/ibztask-team_zh_CN';
import product_zh_CN from '@locale/lanres/entities/product/product_zh_CN';
import taskteam_zh_CN from '@locale/lanres/entities/task-team/task-team_zh_CN';
import file_zh_CN from '@locale/lanres/entities/file/file_zh_CN';
import suitecase_zh_CN from '@locale/lanres/entities/suite-case/suite-case_zh_CN';
import burn_zh_CN from '@locale/lanres/entities/burn/burn_zh_CN';
import subproductplan_zh_CN from '@locale/lanres/entities/sub-product-plan/sub-product-plan_zh_CN';
import storyspec_zh_CN from '@locale/lanres/entities/story-spec/story-spec_zh_CN';
import usertpl_zh_CN from '@locale/lanres/entities/user-tpl/user-tpl_zh_CN';
import branch_zh_CN from '@locale/lanres/entities/branch/branch_zh_CN';
import productstats_zh_CN from '@locale/lanres/entities/product-stats/product-stats_zh_CN';
import ibzprojectmember_zh_CN from '@locale/lanres/entities/ibz-project-member/ibz-project-member_zh_CN';
import action_zh_CN from '@locale/lanres/entities/action/action_zh_CN';
import group_zh_CN from '@locale/lanres/entities/group/group_zh_CN';
import casestep_zh_CN from '@locale/lanres/entities/case-step/case-step_zh_CN';
import dept_zh_CN from '@locale/lanres/entities/dept/dept_zh_CN';
import company_zh_CN from '@locale/lanres/entities/company/company_zh_CN';
import ibzcasestep_zh_CN from '@locale/lanres/entities/ibzcase-step/ibzcase-step_zh_CN';
import taskestimate_zh_CN from '@locale/lanres/entities/task-estimate/task-estimate_zh_CN';
import story_zh_CN from '@locale/lanres/entities/story/story_zh_CN';
import todo_zh_CN from '@locale/lanres/entities/todo/todo_zh_CN';
import subtask_zh_CN from '@locale/lanres/entities/sub-task/sub-task_zh_CN';
import project_zh_CN from '@locale/lanres/entities/project/project_zh_CN';
import history_zh_CN from '@locale/lanres/entities/history/history_zh_CN';
import user_zh_CN from '@locale/lanres/entities/user/user_zh_CN';
import doclib_zh_CN from '@locale/lanres/entities/doc-lib/doc-lib_zh_CN';
import productmodule_zh_CN from '@locale/lanres/entities/product-module/product-module_zh_CN';
import module_zh_CN from '@locale/lanres/entities/module/module_zh_CN';
import testmodule_zh_CN from '@locale/lanres/entities/test-module/test-module_zh_CN';
import productlife_zh_CN from '@locale/lanres/entities/product-life/product-life_zh_CN';
import task_zh_CN from '@locale/lanres/entities/task/task_zh_CN';
import build_zh_CN from '@locale/lanres/entities/build/build_zh_CN';
import testresult_zh_CN from '@locale/lanres/entities/test-result/test-result_zh_CN';
import testsuite_zh_CN from '@locale/lanres/entities/test-suite/test-suite_zh_CN';
import projectteam_zh_CN from '@locale/lanres/entities/project-team/project-team_zh_CN';
import testtask_zh_CN from '@locale/lanres/entities/test-task/test-task_zh_CN';
import ibztaskestimate_zh_CN from '@locale/lanres/entities/ibztask-estimate/ibztask-estimate_zh_CN';
import productline_zh_CN from '@locale/lanres/entities/product-line/product-line_zh_CN';
import testreport_zh_CN from '@locale/lanres/entities/test-report/test-report_zh_CN';
import projectstats_zh_CN from '@locale/lanres/entities/project-stats/project-stats_zh_CN';
import testrun_zh_CN from '@locale/lanres/entities/test-run/test-run_zh_CN';
import ibzmyterritory_zh_CN from '@locale/lanres/entities/ibz-my-territory/ibz-my-territory_zh_CN';
import bug_zh_CN from '@locale/lanres/entities/bug/bug_zh_CN';
import projectmodule_zh_CN from '@locale/lanres/entities/project-module/project-module_zh_CN';
import ibzdoc_zh_CN from '@locale/lanres/entities/ibz-doc/ibz-doc_zh_CN';
import release_zh_CN from '@locale/lanres/entities/release/release_zh_CN';
import dynadashboard_zh_CN from '@locale/lanres/entities/dyna-dashboard/dyna-dashboard_zh_CN';
import components_zh_CN from '@locale/lanres/components/components_zh_CN';
import codelist_zh_CN from '@locale/lanres/codelist/codelist_zh_CN';
import userCustom_zh_CN from '@locale/lanres/userCustom/userCustom_zh_CN';

export default {
    app: {
        commonWords:{
            error: "失败",
            success: "成功",
            ok: "确认",
            cancel: "取消",
            save: "保存",
            codeNotExist: "代码表不存在",
            reqException: "请求异常",
            sysException: "系统异常",
            warning: "警告",
            wrong: "错误",
            rulesException: "值规则校验异常",
            saveSuccess: "保存成功",
            saveFailed: "保存失败",
            deleteSuccess: "删除成功！",
            deleteError: "删除失败！",
            delDataFail: "删除数据失败",
            noData: "暂无数据",
            startsuccess:"启动成功"
        },
        local:{
            new: "新建",
            add: "增加",
        },
        gridpage: {
            choicecolumns: "选择列",
            refresh: "刷新",
            show: "显示",
            records: "条",
            totle: "共",
            noData: "无数据",
            valueVail: "值不能为空",
            notConfig: {
                fetchAction: "视图表格fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图表格createAction参数未配置",
                updateAction: "视图表格updateAction参数未配置",
                loaddraftAction: "视图表格loaddraftAction参数未配置",
            },
            data: "数据",
            delDataFail: "删除数据失败",
            delSuccess: "删除成功!",
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
            notBatch: "批量添加未实现",
            grid: "表",
            exportFail: "数据导出失败",
            sum: "合计",
            formitemFailed: "表单项更新失败",
        },
        list: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图列表createAction参数未配置",
                updateAction: "视图列表updateAction参数未配置",
            },
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
        },
        listExpBar: {
            title: "列表导航栏",
        },
        wfExpBar: {
            title: "流程导航栏",
        },
        calendarExpBar:{
            title: "日历导航栏",
        },
        treeExpBar: {
            title: "树视图导航栏",
        },
        portlet: {
            noExtensions: "无扩展插件",
        },
        tabpage: {
            sureclosetip: {
                title: "关闭提醒",
                content: "表单数据已经修改，确定要关闭？",
            },
            closeall: "关闭所有",
            closeother: "关闭其他",
        },
        fileUpload: {
            caption: "上传",
        },
        searchButton: {
            search: "搜索",
            reset: "重置",
        },
        calendar:{
          today: "今天",
          month: "月",
          week: "周",
          day: "天",
          list: "列",
          dateSelectModalTitle: "选择要跳转的时间",
          gotoDate: "跳转",
          from: "从",
          to: "至",
        },
        // 非实体视图
        views: {
            ibizpms: {
                caption: "iBiz软件生产管理",
                title: "iBiz软件生产管理",
            },
            productportalview: {
                caption: "产品主页",
                title: "产品主页",
            },
            projectportalview: {
                caption: "项目主页",
                title: "项目主页",
            },
            testportalview: {
                caption: "测试主页",
                title: "测试主页",
            },
        },
        utilview:{
            importview:"导入数据",
            warning:"警告",
            info:"请配置数据导入项" 
        },
        menus: {
            zentao: {
                top_menus: "顶部菜单",
                menuitem9: "我的地盘",
                menuitem3: "产品主页",
                menuitem2: "项目主页",
                menuitem1: "测试主页",
                menuitem7: "iBiz软件生产管理",
                left_exp: "左侧分页导航",
                menuitem4: "产品",
                menuitem5: "项目",
                menuitem6: "测试",
                bottom_exp: "底部导航区",
                footer_center: "底部中间菜单",
            },
        },
        formpage:{
            desc1: "操作失败,未能找到当前表单项",
            desc2: "无法继续操作",
            notconfig: {
                loadaction: "视图表单loadAction参数未配置",
                loaddraftaction: "视图表单loaddraftAction参数未配置",
                actionname: "视图表单'+actionName+'参数未配置",
                removeaction: "视图表单removeAction参数未配置",
            },
            saveerror: "保存数据发生错误",
            savecontent: "数据不一致，可能后台数据已经被修改,是否要重新加载数据？",
            valuecheckex: "值规则校验异常",
            savesuccess: "保存成功！",
            deletesuccess: "删除成功！",  
            workflow: {
                starterror: "工作流启动失败",
                startsuccess: "工作流启动成功",
                submiterror: "工作流提交失败",
                submitsuccess: "工作流提交成功",
            },
            updateerror: "表单项更新失败",     
        },
        gridBar: {
            title: "表格导航栏",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "视图多编辑视图面板fetchAction参数未配置",
                loaddraftAction: "视图多编辑视图面板loaddraftAction参数未配置",
            },
        },
        dataViewExpBar: {
            title: "卡片视图导航栏",
        },
        kanban: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
            },
            delete1: "确认要删除 ",
            delete2: "删除操作将不可恢复？",
        },
        dashBoard: {
            handleClick: {
                title: "面板设计",
            },
        },
        dataView: {
            sum: "共",
            data: "条数据",
        },
        chart: {
            undefined: "未定义",
            quarter: "季度",   
            year: "年",
        },
        searchForm: {
            notConfig: {
                loadAction: "视图搜索表单loadAction参数未配置",
                loaddraftAction: "视图搜索表单loaddraftAction参数未配置",
            },
            custom: "存储自定义查询",
            title: "名称",
        },
        wizardPanel: {
            back: "上一步",
            next: "下一步",
            complete: "完成",
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "尊敬的客户您好，您已成功退出系统，将在",
                prompt2: "秒后跳转至",
                logingPage: "登录页",
            },
            appWfstepTraceView: {
                title: "应用流程处理记录视图",
            },
            appWfstepDataView: {
                title: "应用流程跟踪视图",
            },
            appLoginView: {
                username: "用户名",
                password: "密码",
                login: "登录",
            },
        },
    },
    form: {
        group: {
            show_more: "显示更多",
            hidden_more: "隐藏更多"
        }
    },
    entities: {
        productplan: productplan_zh_CN,
        projectproduct: projectproduct_zh_CN,
        case: case_zh_CN,
        ibztaskteam: ibztaskteam_zh_CN,
        product: product_zh_CN,
        taskteam: taskteam_zh_CN,
        file: file_zh_CN,
        suitecase: suitecase_zh_CN,
        burn: burn_zh_CN,
        subproductplan: subproductplan_zh_CN,
        storyspec: storyspec_zh_CN,
        usertpl: usertpl_zh_CN,
        branch: branch_zh_CN,
        productstats: productstats_zh_CN,
        ibzprojectmember: ibzprojectmember_zh_CN,
        action: action_zh_CN,
        group: group_zh_CN,
        casestep: casestep_zh_CN,
        dept: dept_zh_CN,
        company: company_zh_CN,
        ibzcasestep: ibzcasestep_zh_CN,
        taskestimate: taskestimate_zh_CN,
        story: story_zh_CN,
        todo: todo_zh_CN,
        subtask: subtask_zh_CN,
        project: project_zh_CN,
        history: history_zh_CN,
        user: user_zh_CN,
        doclib: doclib_zh_CN,
        productmodule: productmodule_zh_CN,
        module: module_zh_CN,
        testmodule: testmodule_zh_CN,
        productlife: productlife_zh_CN,
        task: task_zh_CN,
        build: build_zh_CN,
        testresult: testresult_zh_CN,
        testsuite: testsuite_zh_CN,
        projectteam: projectteam_zh_CN,
        testtask: testtask_zh_CN,
        ibztaskestimate: ibztaskestimate_zh_CN,
        productline: productline_zh_CN,
        testreport: testreport_zh_CN,
        projectstats: projectstats_zh_CN,
        testrun: testrun_zh_CN,
        ibzmyterritory: ibzmyterritory_zh_CN,
        bug: bug_zh_CN,
        projectmodule: projectmodule_zh_CN,
        ibzdoc: ibzdoc_zh_CN,
        release: release_zh_CN,
        dynadashboard: dynadashboard_zh_CN,
    },
    components: components_zh_CN,
    codelist: codelist_zh_CN,
    userCustom: userCustom_zh_CN,
};