import StoryUIServiceBase from './story-ui-service-base';

/**
 * 需求UI服务对象
 *
 * @export
 * @class StoryUIService
 */
export default class StoryUIService extends StoryUIServiceBase {

    /**
     * Creates an instance of  StoryUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  StoryUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}