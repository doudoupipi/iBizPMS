/**
 * MainDashboard 部件模型
 *
 * @export
 * @class MainDashboardModel
 */
export default class MainDashboardModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MainDashboardModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'lastediteddate',
      },
      {
        name: 'scripteddate',
      },
      {
        name: 'color',
      },
      {
        name: 'path',
      },
      {
        name: 'openeddate',
      },
      {
        name: 'lastrunresult',
      },
      {
        name: 'linkcase',
      },
      {
        name: 'order',
      },
      {
        name: 'howrun',
      },
      {
        name: 'version',
      },
      {
        name: 'scriptedby',
      },
      {
        name: 'openedby',
      },
      {
        name: 'type',
      },
      {
        name: 'status',
      },
      {
        name: 'auto',
      },
      {
        name: 'frequency',
      },
      {
        name: 'title',
      },
      {
        name: 'lasteditedby',
      },
      {
        name: 'reviewedby',
      },
      {
        name: 'deleted',
      },
      {
        name: 'revieweddate',
      },
      {
        name: 'pri',
      },
      {
        name: 'stage',
      },
      {
        name: 'scriptlocation',
      },
      {
        name: 'lastrundate',
      },
      {
        name: 'keywords',
      },
      {
        name: 'scriptstatus',
      },
      {
        name: 'frame',
      },
      {
        name: 'substatus',
      },
      {
        name: 'case',
        prop: 'id',
      },
      {
        name: 'precondition',
      },
      {
        name: 'lastrunner',
      },
      {
        name: 'fromcaseversion',
      },
      {
        name: 'storyversion',
      },
      {
        name: 'fromcaseid',
      },
      {
        name: 'branch',
      },
      {
        name: 'frombug',
      },
      {
        name: 'story',
      },
      {
        name: 'product',
      },
      {
        name: 'lib',
      },
      {
        name: 'module',
      },
      {
        name: 'modulename',
      },
      {
        name: 'storyname',
      },
      {
        name: 'productname',
      },
      {
        name: 'casesteps',
      },
      {
        name: 'tobugcnt',
      },
      {
        name: 'resultcnt',
      },
      {
        name: 'stepcnt',
      },
      {
        name: 'comment',
      },
    ]
  }


}