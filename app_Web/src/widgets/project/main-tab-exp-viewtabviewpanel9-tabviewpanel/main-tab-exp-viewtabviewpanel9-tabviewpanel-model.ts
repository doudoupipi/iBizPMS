/**
 * MainTabExpViewtabviewpanel9 部件模型
 *
 * @export
 * @class MainTabExpViewtabviewpanel9Model
 */
export default class MainTabExpViewtabviewpanel9Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MainTabExpViewtabviewpanel9Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'openedversion',
      },
      {
        name: 'begin',
      },
      {
        name: 'acl',
      },
      {
        name: 'deleted',
      },
      {
        name: 'desc',
      },
      {
        name: 'pm',
      },
      {
        name: 'project',
        prop: 'id',
      },
      {
        name: 'name',
      },
      {
        name: 'substatus',
      },
      {
        name: 'order',
      },
      {
        name: 'rd',
      },
      {
        name: 'whitelist',
      },
      {
        name: 'pri',
      },
      {
        name: 'end',
      },
      {
        name: 'canceleddate',
      },
      {
        name: 'code',
      },
      {
        name: 'catid',
      },
      {
        name: 'statge',
      },
      {
        name: 'canceledby',
      },
      {
        name: 'iscat',
      },
      {
        name: 'openeddate',
      },
      {
        name: 'closedby',
      },
      {
        name: 'type',
      },
      {
        name: 'po',
      },
      {
        name: 'status',
      },
      {
        name: 'days',
      },
      {
        name: 'team',
      },
      {
        name: 'closeddate',
      },
      {
        name: 'openedby',
      },
      {
        name: 'qd',
      },
      {
        name: 'parentname',
      },
      {
        name: 'parent',
      },
      {
        name: 'taskcnt',
      },
      {
        name: 'bugcnt',
      },
      {
        name: 'storycnt',
      },
      {
        name: 'products',
      },
      {
        name: 'branchs',
      },
      {
        name: 'plans',
      },
      {
        name: 'srfarray',
      },
      {
        name: 'comment',
      },
      {
        name: 'period',
      },
      {
        name: 'account',
      },
      {
        name: 'join',
      },
      {
        name: 'hours',
      },
      {
        name: 'role',
      },
      {
        name: 'totalconsumed',
      },
      {
        name: 'totalwh',
      },
      {
        name: 'totalleft',
      },
      {
        name: 'totalestimate',
      },
      {
        name: 'totalhours',
      },
    ]
  }


}